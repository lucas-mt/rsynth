#include "Var.h"
#include "Mgr.h"
#include <algorithm>

using std::vector;

Var::Var(unsigned int id_) : id(id_)
{
	// intentionally left blank
}

unsigned int Var::getId() const
{
	return id;
}

BDD Var::bddVar() const
{
	return m.bddVar(id);
}
	
bool Var::operator==(const Var& other) const
{
	return id == other.id;
}

bool Var::operator<(const Var& other) const
{
	return id < other.id;
}

BDD Var::selectParam(obj<ParamSelector> ps, const BDD& pWitness) const
{
	return ps->selectParam(id, pWitness);
}

BDD Var::compose(const BDD& witness, const BDD& phi) const
{
	return phi.Compose(witness, id);
}

VarList Var::support(const BDD& bdd)
{
	vector<unsigned int> supportIndices = bdd.SupportIndices();
	VarList vars;
	
	for (const auto& i : supportIndices)
	{
		vars.emplace_back(i);
	}
	
	return vars;
}
