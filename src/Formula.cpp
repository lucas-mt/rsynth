#include "Formula.h"

using std::vector;
using std::unordered_set;
using std::unordered_map;

void CNF::addClause(const vector<int>& clause)
{
	clauses.push_back(clause);
}

Instance Formula::buildInstance() const
{
	unordered_map<int, BDD> varBDD;
	
	for (int id : existentialVars)
	{
		varBDD[id] = m.bddVar(id);
	}
	
	for (int id : universalVars)
	{
		varBDD[id] = m.bddVar(id);
	}

	return buildInstance(varBDD);
}

Instance CNF::buildInstance(const unordered_map<int, BDD>& varBDD) const
{
	BDD outputCube = m.bddOne();
	
	for (int id : existentialVars)
	{
		outputCube &= varBDD.at(id);
	}
	
	vector<BDD> factors;
	
	for (auto& clause : clauses)
	{
		BDD factor = m.bddZero();
		
		for (int lit : clause)
		{
			if (lit < 0)
			{
				factor |= !varBDD.at(-lit);
			}
			else
			{
				factor |= varBDD.at(lit);
			}
		}
		
		factors.push_back(factor);
	}
	
	return Instance(outputCube, move(factors));
}

obj<Formula> CNF::abstract()
{
	obj<SupportFormula> sf = newObj<SupportFormula>(universalVars, existentialVars);
	
	for (auto& clause : clauses)
	{
		vector<int> cube;
		
		for (int lit : clause)
		{
			cube.push_back(abs(lit));
		}
		
		sf->addCube(move(cube));
	}
	
	return sf;
}

const Graph CNF::gaifmanGraph() const
{
	unordered_set<int> vars;
	vars.insert(universalVars.begin(), universalVars.end());
	vars.insert(existentialVars.begin(), existentialVars.end());

	Graph g(move(vars));
	
	for (auto& clause : clauses)
	for (auto l1 = clause.begin(); l1 != clause.end(); l1++)
	for (auto l2 = next(l1); l2 != clause.end(); l2++)
	{
		int var1 = abs(*l1);
		int var2 = abs(*l2);
		
		g.addEdge(var1, var2);
	}
	
	return g;
}

void SupportFormula::addCube(vector<int> cube)
{
	cubes.push_back(cube);
}

Instance SupportFormula::buildInstance(const unordered_map<int, BDD>& varBDD) const
{
	BDD outputCube = m.bddOne();
	
	for (int id : existentialVars)
	{
		outputCube &= varBDD.at(id);
	}
	
	vector<BDD> factors;
	
	for (auto& cube : cubes)
	{
		BDD factor = m.bddOne();
		
		for (int lit : cube)
		{
			factor &= varBDD.at(lit);
		}
		
		factors.push_back(factor);
	}
	
	Instance inst = Instance(outputCube, move(factors));
	
	return inst;
}
const Graph SupportFormula::gaifmanGraph() const
{
	unordered_set<int> vars;
	vars.insert(universalVars.begin(), universalVars.end());
	vars.insert(existentialVars.begin(), existentialVars.end());

	Graph g(move(vars));
	
	for (auto& cube : cubes)
	for (auto l1 = cube.begin(); l1 != cube.end(); l1++)
	for (auto l2 = next(l1); l2 != cube.end(); l2++)
	{
		int var1 = abs(*l1);
		int var2 = abs(*l2);
		
		g.addEdge(var1, var2);
	}
	
	return g;
}

obj<Formula> SupportFormula::abstract()
{
	return obj<Formula>(this);
}
