#include "Ordering.h"
#include "Mgr.h"

#include <deque>
#include <vector>

using std::deque;
using std::vector;
using std::unordered_map;
using std::pair;
using std::make_pair;
using std::move;

obj<Ordering> Ordering::native()
{
	return newObj<NativeOrdering>();
}

obj<Ordering> Ordering::mcs()
{
	return newObj<MCSOrdering>();
}

Instance MCSOrdering::buildInstance(obj<Formula> form)
{
	Graph g = form->gaifmanGraph();

	deque<int> ordering;
	
	unordered_map<int, int> count;
	
	auto it = g.beginVertices();
	if (it == g.endVertices()) return form->buildInstance(); /* empty graph */
	int bestVertex = *it; /* would segfault if graph was empty */
	
	for (it++; it != g.endVertices(); it++)
	{
		count[*it] = 0;
	}
	
	int bestCount;
	
	do
	{
		ordering.push_front(bestVertex);
		count.erase(bestVertex);
	
		for (auto n = g.beginNeighbors(bestVertex)
			; n != g.endNeighbors(bestVertex)
			; n++)
		{
			auto entry = count.find(*n);
			
			if (entry != count.end())
			{
				entry->second++;
			}
		}
		
		bestCount = -1;
		
		for (pair<int, int> entry : count)
		if (entry.second > bestCount)
		{
			bestVertex = entry.first;
			bestCount = entry.second;
		}
	}
	while (bestCount != -1);
	
	unordered_map<int, BDD> varBDD;
	
	for (int v : ordering)
	{
		varBDD[v] = m.bddVar();
	}
	
	return form->buildInstance(varBDD);
}

Instance NativeOrdering::buildInstance(obj<Formula> form)
{
	return form->buildInstance();
}
