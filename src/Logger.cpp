#include "Logger.h"

#include <iostream>

using std::vector;
using std::endl;
using std::ostream;
using std::move;

Logger::Logger(ostream& os) : out(os) { }

void Logger::log(const BDD& pre, const BDD& cluster, const BDD& conj, const BDD& quant)
{
	vector<BDD> data(4);
	
	data[0] = pre;
	data[1] = cluster;
	data[2] = conj;
	data[3] = quant;
	
	log(move(data));
}

obj<Logger> Logger::voidLogger(ostream& os)
{
	return newObj<VoidLogger>(os);
}

obj<Logger> Logger::bddLogger(ostream& os)
{
	return newObj<BDDLogger>(os);
}

obj<Logger> Logger::sizeLogger(ostream& os)
{
	return newObj<SizeLogger>(os);
}

VoidLogger::VoidLogger(ostream& os) : Logger(os) { }

BDDLogger::BDDLogger(ostream& os) : Logger(os) { }

SizeLogger::SizeLogger(ostream& os) : Logger(os) { }

void VoidLogger::log(const std::vector<BDD>& data)
{
	// Nop
}

void BDDLogger::log(const std::vector<BDD>& data)
{
	out << endl << "Current BDD: " << data[0] << endl;
	out << "Next cluster: " << data[1] << endl;
	out << "Conjunction: " << data[2] << endl;
	out << "Quantified: " << data[3] << endl << endl;
}

void SizeLogger::log(const std::vector<BDD>& data)
{
	/*out << "Current BDD: " << data[0].nodeCount() << " nodes, " << data[0].SupportSize() << " variables" << endl;
	out << "Next cluster: " << data[1].nodeCount() << " nodes, " << data[1].SupportSize() << " variables" << endl;
	out << "Conjunction: " << data[2].nodeCount() << " nodes, " << data[2].SupportSize() << " variables" << endl;
	out << "Quantified: " << data[3].nodeCount() << " nodes, " << data[3].SupportSize() << " variables" << endl << endl;*/
	
	out << data[0].nodeCount() << " " << data[0].SupportSize() << " ";
	out << data[1].nodeCount() << " " << data[1].SupportSize() << " ";
	out << data[2].nodeCount() << " " << data[2].SupportSize() << " ";
	out << data[3].nodeCount() << " " << data[3].SupportSize() << endl;
}
