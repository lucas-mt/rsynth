#include "Implementation.h"
#include "Mgr.h"

#include <algorithm>

using std::vector;
using std::unordered_map;
using std::move;

Implementation::Implementation(vector<BDD> pre, unordered_map<Var, BDD, VarHash> witn)
	: precondition(move(pre)), witnesses(move(witn))
{
	// intentionally left blank
}

const BDD& Implementation::operator[](const Var& v)
{
	return witnesses[v];
}

template<class K, class V, class Hash>
vector<V> values(unordered_map<K, V, Hash> m)
{
	vector<V> v;
	
	for (auto entry : m)
	{
		v.push_back(entry.second);
	}
	
	return v;
}

void Implementation::dumpDot() const
{
	m.DumpDot(precondition);
	m.DumpDot(values(witnesses));
}

int Implementation::nodeCount() const
{
	return m.nodeCount(precondition) + m.nodeCount(values(witnesses));
}

Result Implementation::eval(const unordered_map<Var, bool, VarHash>& input) const
{
	unsigned int maxIndex = max_element(input.begin(), input.end())->first.getId();
	int* inputArray = new int[maxIndex + 1];
	
	for (const auto& entry : input)
	{
		inputArray[entry.first.getId()] = entry.second;
	}
	
	for (const BDD& pre : precondition)
	if (pre.Eval(inputArray).IsZero())
		return Result();

	vector<bool> output;

	for (const auto& witness : witnesses)
	{
		output.push_back(witness.second.Eval(inputArray).IsOne());
	}

	delete[] inputArray;

	return Result(output);
}
