#include "ParamSelector.h"

#include "Mgr.h"

BDD positiveCofactor(BDD phi, int id)
{
	return phi.Compose(m.bddOne(), id);
}

BDD negativeCofactor(BDD phi, int id)
{
	return phi.Compose(m.bddZero(), id);
}

obj<ParamSelector> ParamSelector::defaultOne()
{
	return newObj<Default1Selector>();
}

obj<ParamSelector> ParamSelector::defaultZero()
{
	return newObj<Default0Selector>();
}

obj<ParamSelector> ParamSelector::greedy()
{
	return newObj<GreedySelector>();
}

BDD Default1Selector::selectParam(int id, const BDD& pWitness) const
{
	return m.bddOne();
}

BDD Default0Selector::selectParam(int id, const BDD& pWitness) const
{
	return m.bddZero();
}

BDD GreedySelector::selectParam(int id, const BDD& pWitness) const
{
	BDD def1 = positiveCofactor(pWitness, id);
	BDD def0 = negativeCofactor(pWitness, id);

	if (def1.nodeCount() < def0.nodeCount())
	{
		return m.bddOne();
	}
	else
	{
		return m.bddZero();
	}
}
