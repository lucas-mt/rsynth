#include "Args.h"
#include <stdexcept>
#include <iostream>

using std::cout;
using std::endl;
using std::invalid_argument;
using std::runtime_error;

int main (int argc, char* argv[])
{
	Args args(argc - 1, argv + 1);
	
	try
	{
		auto command = args.buildCommand();
		
		command->execute();
	}
	catch (const invalid_argument& e)
	{
		cout << e.what() << "." << endl << endl;
		
		cout << "See README file for instructions." << endl;
	}
	catch (const runtime_error& e)
	{
		cout << e.what() << "." << endl << endl;
	}
}
