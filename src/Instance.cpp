#include "Instance.h"

using std::vector;
using std::move;

Instance::Instance(const BDD& oc, std::vector<BDD> f)
	: outputCube(oc), factors(move(f)) { }
	
/*
Instance Instance::fromCNF(const CNF& cnf)
{
	BDD outputCube = m.bddOne();
	
	for (int id : cnf.boundVars)
	{
		outputCube &= m.bddVar(id);
	}
	
	vector<BDD> factors;
	
	for (auto clause = cnf.begin(); clause != cnf.end(); clause++)
	{
		BDD factor = m.bddOne();
		
		for (int lit : *clause)
		{
			if (lit < 0)
			{
				factor |= !m.bddVar(-lit);
			}
			else
			{
				factor |= m.bddVar(lit);
			}
		}
		
		factors.push_back(factor);
	}
	
	return Instance(outputCube, factors);
}
*/
