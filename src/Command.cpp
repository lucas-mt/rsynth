#include "Command.h"
#include "Mgr.h"

#include <iostream>
#include <chrono>
#include <random>

using std::unordered_map;
using std::vector;
using std::chrono::system_clock;
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::default_random_engine;
using std::bernoulli_distribution;
using std::cout;
using std::endl;
using std::flush;

void MeasureCmd::execute() const
{
	auto start = system_clock::now();
	
	Instance instance = ordering->buildInstance(spec);
		
	obj<ClusterScheduler> scheduler = clustering->run(instance.factors, instance.outputCube);
	
	auto duration = duration_cast<milliseconds>(system_clock::now() - start);
	
	vector<Cluster> clusters = scheduler->allClusters();

	cout << duration.count() << " " << clusters.size() << " " << Cluster::nodeCount(clusters) << " " << flush;

	start = system_clock::now();

	Implementation implementation = scheduler->run(synthesizer, logger);

	duration = duration_cast<milliseconds>(system_clock::now() - start);

	cout << duration.count() << " " << implementation.nodeCount() << endl;
}

void DumpDotCmd::execute() const
{
	Instance instance = ordering->buildInstance(spec);
		
	obj<ClusterScheduler> scheduler = clustering->run(instance.factors, instance.outputCube);
	
	Cluster::dumpDot(scheduler->allClusters());
	
	Implementation implementation = scheduler->run(synthesizer, logger);

	implementation.dumpDot();
}

void TestCmd::execute() const
{
	Instance instance = ordering->buildInstance(spec);
		
	obj<ClusterScheduler> scheduler = clustering->run(instance.factors, instance.outputCube);
	Implementation implementation = scheduler->run(synthesizer, logger);
		
	vector<Cluster> clusters = scheduler->allClusters();
	BDD inputCube = Cluster::totalSupport(clusters).Restrict(Cluster::quantifiableVars(clusters));
	
	unordered_map<Var, bool, VarHash> input;
	
	default_random_engine rng;
	bernoulli_distribution choose(0.5);
	
	for (int k = 0; k < 10; k++)
	{
		for (Var v : Var::support(inputCube))
		{
			input[v] = choose(rng);
			
			cout << input[v];
		}
		
		cout << " -> ";
	
		Result result = implementation.eval(input);
		
		for (int i = 0; i < result.size(); i++)
		{
			cout << result[i];
		}
		
		cout << endl;
	}
}

void ValidateCmd::execute() const
{
	Instance instance = ordering->buildInstance(spec);
		
	obj<ClusterScheduler> scheduler = clustering->run(instance.factors, instance.outputCube);
	
	Implementation implementation = scheduler->run(synthesizer, logger);
	BDD rest = m.bddOne();
	
	for (const auto& cluster : scheduler->allClusters())
	{
		rest &= cluster.formula;
	
		for (Var var : Var::support(cluster.quantifiable))
		{
			rest = var.compose(implementation[var], rest);
		}
	}
	
	for (const BDD& pre : implementation.precondition)
		rest |= !pre;
	
	if (rest.IsOne())
	{
		cout << "Correct!" << endl;
	}
	else
	{
		cout << "Incorrect: " << rest << endl;
	}
}
	
obj<Command> Command::measure(obj<Formula> f, obj<Ordering> o, obj<Clustering> c, obj<Synthesizer> s, obj<Logger> l)
{
	return newObj<MeasureCmd>(f, o, c, s, l);
}
	
obj<Command> Command::dumpDot(obj<Formula> f, obj<Ordering> o, obj<Clustering> c, obj<Synthesizer> s, obj<Logger> l)
{
	return newObj<DumpDotCmd>(f, o, c, s, l);
}

obj<Command> Command::test(obj<Formula> f, obj<Ordering> o, obj<Clustering> c, obj<Synthesizer> s, obj<Logger> l)
{
	return newObj<TestCmd>(f, o, c, s, l);
}

obj<Command> Command::validate(obj<Formula> f, obj<Ordering> o, obj<Clustering> c, obj<Synthesizer> s, obj<Logger> l)
{
	return newObj<ValidateCmd>(f, o, c, s, l);
}
