#include "Graph.h"

using std::unordered_set;
using std::move;

Graph::Graph(unordered_set<int> vars) : vertices(move(vars))
{
	for (int var : vars)
	{
		adjacencyMap[var] = unordered_set<int>();
	}
}

void Graph::addEdge(int var1, int var2)
{
	adjacencyMap[var1].insert(var2);
	adjacencyMap[var2].insert(var1);
}

unordered_set<int>::const_iterator Graph::beginVertices() const
{
	return vertices.begin();
}

unordered_set<int>::const_iterator Graph::endVertices() const
{
	return vertices.end();
}

unordered_set<int>::const_iterator Graph::beginNeighbors(int var)
{
	return adjacencyMap[var].begin();
}

unordered_set<int>::const_iterator Graph::endNeighbors(int var)
{
	return adjacencyMap[var].end();
}
