#include "Args.h"
#include "Mgr.h"

#include <iostream>

using std::vector;
using std::string;
using std::unordered_map;
using std::invalid_argument;
using std::cout;
	
string Args::retrieve(string option) const
{
	auto it = args.find(option);
	
	if (it == args.end())
		throw invalid_argument("No -" + option + " option provided");
		
	return it->second;
}

obj<Formula> Args::getSpecification() const
{
	string representation = retrieve("r");
	string path = retrieve("s");
	
	if (representation == "qdimacs")
	{
		return DimacsReader::readAEDimacs(path);
	}
	//else if (representation == "qdimacs-support")
	//{
	//	return DimacsReader::readAEDimacs(path)->abstract();
	//}
	else
		throw invalid_argument("Invalid representation type: " + representation);
}

obj<Ordering> Args::getOrdering() const
{
	string ordering = retrieve("o");
	
	if (ordering == "native")
		return Ordering::native();
	else if (ordering == "mcs")
		return Ordering::mcs();
	else
		throw invalid_argument("Invalid ordering: " + ordering);
}

obj<Clustering> Args::getClustering() const
{
	string clustering = retrieve("c");
	
	if (clustering == "none")
		return Clustering::none();
	else if (clustering == "random")
		return Clustering::random();
	else if (clustering == "monolithic")
		return Clustering::monolithic();
	else if (clustering == "bouquets")
		return Clustering::bouquets();
	else if (clustering == "bucket")
		return Clustering::bucket();
	else if (clustering == "forward")
		return Clustering::forward();
	else if (clustering == "backward")
		return Clustering::backward();
	//else if (clustering == "lazy")
	//	return Clustering::lazy();
	else
		throw invalid_argument("Invalid clustering strategy: " + clustering);
}

obj<ParamSelector> Args::getParamSelector() const
{
	string selection = retrieve("w");
	
	if (selection == "def1")
		return ParamSelector::defaultOne();
	else if (selection == "def0")
		return ParamSelector::defaultZero();
	else if (selection == "greedy")
		return ParamSelector::greedy();
	else
		throw invalid_argument("Invalid witness selection method: " + selection);
}

obj<Logger> Args::getLogger() const
{
	return Logger::voidLogger(cout);
	
	/*
	string logging = retrieve("l");
	
	if (logging == "void")
		return Logger::voidLogger(cout);
	else if (logging == "size")
		return Logger::sizeLogger(cout);
	else if (logging == "bdd")
		return Logger::bddLogger(cout);
	else
		throw invalid_argument("Invalid logging option: " + logging);
	*/
}

obj<Synthesizer> Args::getSynthesizer() const
{
	obj<ParamSelector> selector = getParamSelector();
	
	string methodName = retrieve("m");
	
	if (methodName == "booles")
	{
		return Synthesizer::boolesMethod(selector);
	}
	else
	{
		throw invalid_argument("Invalid synthesis method: " + methodName);
	}
}

obj<Command> Args::buildCommand() const
{
	string execMode = retrieve("x");
	
	if (execMode == "measure")
		return Command::measure(getSpecification(), getOrdering(), getClustering(), getSynthesizer(), getLogger());
	else if (execMode == "dump")
		return Command::dumpDot(getSpecification(), getOrdering(), getClustering(), getSynthesizer(), getLogger());
	else if (execMode == "test")
		return Command::test(getSpecification(), getOrdering(), getClustering(), getSynthesizer(), getLogger());
	else if (execMode == "validate")
		return Command::validate(getSpecification(), getOrdering(), getClustering(), getSynthesizer(), getLogger());
	else
		throw invalid_argument("Invalid execution mode: " + execMode);
};

Args::Args(int argc, char* argv[])
{
	for (int i = 0; i < argc; i += 2)
	{
		if (argv[i][0] != '-')
			throw invalid_argument("Argument " + string(argv[i]) + " not preceded by command-line option");
			
		string option(argv[i] + 1);
		
		if (i + 1 == argc)
			throw invalid_argument("Option " + option + " without argument");
		
		string arg(argv[i + 1]);
		
		args[option] = arg;
	}
}
