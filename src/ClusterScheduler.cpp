#include "ClusterScheduler.h"
#include "Mgr.h"

#include <unordered_map>
#include <unordered_set>
#include <algorithm>

using std::vector;
using std::move;
using std::unordered_map;
using std::unordered_set;

BDD Cluster::quantifiableVars(const std::vector<Cluster>& clusters)
{
	BDD vars = m.bddOne();
	
	for (const Cluster& cluster : clusters)
	{
		vars &= cluster.quantifiable;
	}
	
	return vars;
}

int Cluster::nodeCount(const std::vector<Cluster>& clusters)
{
	int count = 0;
	
	for (const Cluster& cluster : clusters)
	{
		count += cluster.formula.nodeCount();
	}
	
	return count;
}

BDD Cluster::totalSupport(const std::vector<Cluster>& clusters)
{
	BDD support = m.bddOne();
	
	for (const Cluster& cluster : clusters)
	{
		support &= cluster.formula.Support();
	}
	
	return support;
}

void Cluster::dumpDot(const std::vector<Cluster>& clusters)
{
	for (const auto& cluster : clusters)
	{
		m.DumpDot(vector<BDD>(1, cluster.formula));
	}
}

LinearScheduler::LinearScheduler(vector<Cluster> clusters_, vector<BDD> assumptions_)
	: clusters(clusters_), assumptions(assumptions_) { }

Implementation LinearScheduler::run(obj<Synthesizer> synthesizer, obj<Logger> logger)
{
	vector<BDD> precondition = assumptions;
	BDD pre = m.bddOne();
	BDD outputVariables = Cluster::quantifiableVars(allClusters());
	BDD quantifiedCube = m.bddOne();
	unordered_map<Var, BDD, VarHash> witnesses;

	for (const auto& cluster : clusters)
	{
		BDD b = cluster.formula;
		BDD quantifiableSupport = cluster.quantifiable;
		
		BDD before = pre;
		BDD conjunction = before & b;
		
		pre = synthesizer->run(conjunction, quantifiableSupport, witnesses);
		
		logger->log(before, b, conjunction, pre);
		
		BDD inputSupport = pre.Support().Restrict(outputVariables);
		
		if (pre.Support().Restrict(inputSupport).IsOne())
		{
			precondition.push_back(pre);
			
			pre = m.bddOne();
		}

		for (Var previous : Var::support(quantifiedCube))
		for (Var current : Var::support(quantifiableSupport))
		{
			witnesses[previous] = current.compose(witnesses[current], witnesses[previous]);
		}
		
		quantifiedCube &= quantifiableSupport;
	}
	
	return Implementation(move(precondition), move(witnesses));
}

vector<Cluster> LinearScheduler::allClusters() const
{
	vector<Cluster> c;
	c.reserve(clusters.size() + assumptions.size());
	
	for (const BDD& assumption : assumptions)
	{
		c.push_back(Cluster(assumption, m.bddOne()));
	}
	
	for (const auto& cluster : clusters)
	{
		c.push_back(cluster);
	}
	
	return c;
}

LazyScheduler::LazyScheduler(vector<Cluster> clusters_, vector<BDD> assumptions_)
	: clusters(clusters_), assumptions(assumptions_) { }

Implementation LazyScheduler::run(obj<Synthesizer> __unused__, obj<Logger> __also_unused__)
{
	vector<BDD> precondition = assumptions;
	BDD outputVariables = Cluster::quantifiableVars(allClusters());
	unordered_map<Var, BDD, VarHash> witnesses;

	unordered_map<Var, unordered_set<int>, VarHash> reverseSupport;

	for (auto& cluster : clusters)
	{
		BDD b = cluster.formula;
		
		for (Var v : Var::support(b))
		{
			reverseSupport[v].insert(precondition.size());
		}
		
		precondition.push_back(b);
	}
	
	for (Var v : Var::support(outputVariables))
	{
		unordered_set<int>& indices = reverseSupport[v];
		
		BDD witness = m.bddOne();
		
		for (int i : indices)
		{
			witness &= v.compose(m.bddOne(), precondition[i]);
		}
		
		for (auto& previous : witnesses)
		{
			previous.second = v.compose(witness, previous.second);
		}
		
		witnesses[v] = witness;
		
		for (int i : indices)
		{
			BDD oldSupport = precondition[i].Support();
			
			precondition[i] = v.compose(witness, precondition[i]);
			
			BDD newSupport = precondition[i].Support();
			
			for (Var v : Var::support(newSupport.Restrict(oldSupport)))
			{
				reverseSupport[v].insert(i);
			}
		}
	}
	
	return Implementation(move(precondition), move(witnesses));
}

vector<Cluster> LazyScheduler::allClusters() const
{
	vector<Cluster> c;
	c.reserve(clusters.size() + assumptions.size());
	
	for (const BDD& assumption : assumptions)
	{
		c.push_back(Cluster(assumption, m.bddOne()));
	}
	
	for (const auto& cluster : clusters)
	{
		c.push_back(cluster);
	}
	
	return c;
}

/*
Implementation TreeScheduler::run(obj<Synthesizer> synthesizer, obj<Logger> logger)
{
	vector<BDD> precondition = assumptions;
	BDD quantifiedCube = m.bddOne();
	unordered_map<Var, BDD, VarHash> witnesses;
	
	unordered_map<Var, vector<size_t>> occurrences;
	
	for (const Cluster& cluster : clusters)
	for (const Var& v : Var::support(cluster[i].quantifiable))
	{
		occurrences[v].push_back(?);
	}
	
	vector<Var> heap = keys(occurrences);
	
	make_heap(heap.begin(), heap.end(),
		[] (const Var& v1, const Var& v2) -> bool
		{
			return occurrences[v1].size() > occurrences[v2].size();
		});
	
	while (!heap.empty())
	{
		
	}
}
*/
