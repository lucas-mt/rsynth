#include <fstream>
#include <stdexcept>
#include <vector>
#include <unordered_set>

#include "DimacsReader.h"
#include "Mgr.h"

using std::vector;
using std::string;
using std::ifstream;
using std::pair;
using std::make_pair;
using std::runtime_error;
using std::unordered_set;

void skipComments(ifstream& in)
{
	string line;

	while (in.peek() == 'c')
	{
		getline(in, line);
	}
	
	if (in.eof())
	{
		throw runtime_error("Unexpected end of file while reading DIMACS file");
	}
}

pair<int, int> readHeader(ifstream& in)
{
	string p, cnf; in >> p >> cnf;
	
	if (p != "p" || cnf != "cnf")
	{
		throw runtime_error("Incorrect format of DIMACS file: expected \"p cnf\", got \"" + p + " " + cnf + "\"");
	}
	
	int nVars, nClauses;
	in >> nVars >> nClauses;
	
	return make_pair(nVars, nClauses);
}

unordered_set<int> readQuantifiedSet(ifstream& in, string quantifier)
{
	string q; in >> q;
	
	if (q != quantifier)
	{
		throw runtime_error("Incorrect format of DIMACS file: expected \"" + quantifier + "e\", got \"" + q + "\"");
	}
	
	unordered_set<int> eVars;
	int v; in >> v;
	
	while (!in.eof() && v != 0)
	{
		eVars.insert(v);
		in >> v;
	}
	
	if (in.eof())
	{
		throw runtime_error("Unexpected end of file while reading DIMACS file");
	}
	
	return eVars;
}

void readClauses(obj<CNF> cnf, ifstream& in, int nClauses)
{
	for (int i = 0; i < nClauses; i++)
	{
		vector<int> clause;
		int lit;
		
		in >> lit;
		
		while (lit != 0)
		{
			clause.push_back(lit);
				
			in >> lit;
		}
		
		cnf->addClause(clause);
	}
}

const obj<CNF> DimacsReader::readAEDimacs(const string& path)
{
	ifstream in(path);
	
	if (!in.is_open())
		throw runtime_error("Unable to open file " + path);

	skipComments(in);
	
	pair<int, int> counts = readHeader(in);
	
	unordered_set<int> universalVars = readQuantifiedSet(in, "a");
	unordered_set<int> existentialVars = readQuantifiedSet(in, "e");
	
	obj<CNF> cnf = newObj<CNF>(move(universalVars), move(existentialVars));
	readClauses(cnf, in, counts.second);
	
	in.close();
	
	return cnf;
}
