#include "Result.h"

using std::vector;
using std::move;

Result::Result() { realizable = false; }
Result::Result(vector<bool> out) : output(move(out)) { realizable = true; }

bool Result::isRealizable() { return realizable; }

size_t Result::size() { return output.size(); }

bool Result::operator[](size_t i) { return output[i]; }
