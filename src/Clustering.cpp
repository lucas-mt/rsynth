#include "Clustering.h"
#include "Mgr.h"

#include <map>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <algorithm>
#include <random>
#include <chrono>

using std::vector;
using std::less;
using std::greater;
using std::map;
using std::unordered_map;
using std::unordered_set;
using std::move;
using std::default_random_engine;
using std::shuffle;
using std::chrono::system_clock;

obj<Clustering> Clustering::none()
{
	return newObj<NoClustering>();
}
	
obj<Clustering> Clustering::random()
{
	return newObj<RandomReordering>();
}
	
obj<Clustering> Clustering::monolithic()
{
	return newObj<MonolithicClustering>();
}
	
obj<Clustering> Clustering::bouquets()
{
	return newObj<BouquetsClustering>();
}
	
obj<Clustering> Clustering::bucket()
{
	return newObj<BucketClustering>();
}
	
obj<Clustering> Clustering::forward()
{
	return newObj<ForwardClustering>();
}
	
obj<Clustering> Clustering::backward()
{
	return newObj<BackwardClustering>();
}

obj<Clustering> Clustering::lazy()
{
	return newObj<LazyClustering>();
}

obj<ClusterScheduler> NoClustering::run(const vector<BDD>& specification, const BDD& outputCube) const
{
	vector<Cluster> clusters;
	vector<BDD> assumptions;
	BDD remainingOutputs = m.bddOne();
	
	for (auto it = specification.begin(); it != specification.end(); it++)
	{
		BDD support = it->Support();
		BDD inputSupport = support.Restrict(outputCube);
		
		if (support == inputSupport)
		{
			assumptions.push_back(*it);
		}
		else
		{
			BDD quantifiableSupport = support.Restrict(remainingOutputs & inputSupport);
		
			clusters.push_back(Cluster(*it, quantifiableSupport));
		
			remainingOutputs &= quantifiableSupport;
		}
	}
	
	reverse(clusters.begin(), clusters.end());
	
	return newObj<LinearScheduler>(move(clusters), move(assumptions));
}

obj<ClusterScheduler> RandomReordering::run(const vector<BDD>& specification, const BDD& outputCube) const
{
	vector<Cluster> clusters;
	vector<BDD> assumptions;
	BDD remainingOutputs = m.bddOne();
	
	vector<BDD> workingCopy(specification);
	
	unsigned seed = system_clock::now().time_since_epoch().count();
	auto engine = default_random_engine(seed);
	shuffle(workingCopy.begin(), workingCopy.end(), engine);
	
	for (auto it = workingCopy.begin(); it != workingCopy.end(); it++)
	{
		BDD support = it->Support();
		BDD inputSupport = support.Restrict(outputCube);
		
		if (support == inputSupport)
		{
			assumptions.push_back(*it);
		}
		else
		{
			BDD quantifiableSupport = support.Restrict(remainingOutputs & inputSupport);
		
			clusters.push_back(Cluster(*it, quantifiableSupport));
		
			remainingOutputs &= quantifiableSupport;
		}
	}
	
	reverse(clusters.begin(), clusters.end());
	
	return newObj<LinearScheduler>(move(clusters), move(assumptions));
}

obj<ClusterScheduler> MonolithicClustering::run(const vector<BDD>& specification, const BDD& outputCube) const
{
	BDD monolithic = m.bddOne();
	
	for (const BDD& factor : specification)
	{
		monolithic &= factor;
	}

	vector<Cluster> clusters(1, Cluster(monolithic, outputCube));
	
	return newObj<LinearScheduler>(move(clusters), vector<BDD>());
}

template<class Compare>
obj<ClusterScheduler> rankClustering(const vector<BDD>& specification, const BDD& outputCube)
{
	map<int, BDD, Compare> byRank;
	BDD inputCube = m.bddOne();
	vector<BDD> assumptions;

	for (const BDD& b : specification)
	{
		BDD support = b.Support();
		BDD inputSupport = support.Restrict(outputCube);
		inputCube &= inputSupport;
		
		vector<unsigned int> outputIndices = support.Restrict(inputSupport).SupportIndices();
		
		if (outputIndices.size() == 0)
		{
			assumptions.push_back(b);
		}
		else
		{
			int rank = *max_element(outputIndices.begin(), outputIndices.end());
	
			if (byRank.find(rank) == byRank.end())
				byRank[rank] = b;
			else
				byRank[rank] &= b;
		}
	}
	
	vector<Cluster> clusters;
	BDD remainingVariables = inputCube;
	
	for (auto it = byRank.rbegin(); it != byRank.rend(); it++)
	{
		BDD cluster = it->second;
		
		BDD quantifiableSupport = cluster.Support().Restrict(remainingVariables);
		
		remainingVariables &= quantifiableSupport;
		
		clusters.push_back(Cluster(move(cluster), move(quantifiableSupport)));
	}
	
	reverse(clusters.begin(), clusters.end());
	
	return newObj<LinearScheduler>(move(clusters), move(assumptions));
}

obj<ClusterScheduler> BouquetsClustering::run(const vector<BDD>& specification, const BDD& outputCube) const
{
	return rankClustering<less<int>>(specification, outputCube);
}

obj<ClusterScheduler> BucketClustering::run(const vector<BDD>& specification, const BDD& outputCube) const
{
	return rankClustering<greater<int>>(specification, outputCube);
}

template<class Value>
class Entry
{
	public:
	
	vector<size_t> key;
	Value value;
	
	Entry(vector<size_t> k, Value v) : key(move(k)), value(move(v)) {}
	
	bool operator==(const Entry<Value>& other) const
	{
		return key == other.key;
	}
	
	bool operator<(const Entry<Value>& other) const
	{
		return key < other.key;
	}
	
	bool operator>(const Entry<Value>& other) const
	{
		return key > other.key;
	}
	
	bool operator<=(const Entry<Value>& other) const
	{
		return key <= other.key;
	}
	
	bool operator>=(const Entry<Value>& other) const
	{
		return key >= other.key;
	}
};

obj<ClusterScheduler> ForwardClustering::run(const vector<BDD>& specification, const BDD& outputCube) const
{
	vector<Cluster> clusters;
	vector<BDD> assumptions;

	size_t k = specification.size();

	vector<BDD> outputSupport(k); // support with only output variables
	unordered_set<size_t> remainingClusters; // clusters that have not been conjoined
	unordered_map<Var, vector<size_t>, VarHash> occurrences; // list of clusters in which each variable appears
	
	for (size_t i = 0; i < k; i++)
	{
		BDD support = specification[i].Support();
		BDD inputSupport = support.Restrict(outputCube);
		outputSupport[i] = support.Restrict(inputSupport);
		
		vector<Var> varSupport = Var::support(outputSupport[i]);
		
		if (varSupport.size() == 0)
		{
			assumptions.push_back(specification[i]);
		}
		else
		{
			remainingClusters.insert(i);
			
			for (Var v : Var::support(outputSupport[i]))
			{
				occurrences[v].push_back(i);
			}
		}
	}
		
	while (!remainingClusters.empty())
	{
		vector<Entry<size_t>> candidates; // all remaining clusters, sorted by key
		
		for (size_t i : remainingClusters)
		{
			size_t reoccurring = 0;
			vector<Var> varSupport = Var::support(outputSupport[i]);
			
			for (Var v : varSupport) // count variables that appear in other clusters
				if (occurrences[v].size() > 1)
					reoccurring++;
			
			// sort first by number of unique variables, then by number of shared variables
			vector<size_t> key = { varSupport.size() - reoccurring, reoccurring };
			candidates.emplace_back(move(key), i);
		}
	
		sort(candidates.begin(), candidates.end());
		
		size_t index = candidates.rbegin()->value;
		remainingClusters.erase(index);
		
		BDD quantifiable = m.bddOne();
		
		for (Var v : Var::support(outputSupport[index]))
		{
			vector<size_t>& occur = occurrences[v];
			occur.erase(remove(occur.begin(), occur.end(), index), occur.end());
			
			if (occur.size() == 0) quantifiable &= v.bddVar();
		}
		
		clusters.emplace_back(specification[index], quantifiable);
	}
	
	return newObj<LinearScheduler>(move(clusters), vector<BDD>());
}

obj<ClusterScheduler> BackwardClustering::run(const vector<BDD>& specification, const BDD& outputCube) const
{
	vector<Cluster> clusters;
	vector<BDD> assumptions;

	size_t k = specification.size();

	vector<BDD> support(k);
	unordered_set<size_t> remainingClusters; // clusters that have not been conjoined
	unordered_map<Var, vector<size_t>, VarHash> occurrences; // list of clusters in which each variable appears
	
	for (size_t i = 0; i < k; i++)
	{
		support[i] = specification[i].Support();
		BDD inputSupport = support[i].Restrict(outputCube);
		
		if (inputSupport == support[i])
		{
			assumptions.push_back(specification[i]);
		}
		else
		{
			remainingClusters.insert(i);
		
			for (Var v : Var::support(support[i]))
			{
				occurrences[v].push_back(i);
			}
		}
	}
	
	BDD precedent = m.bddOne(); // variables that have already been conjoined
		
	while (!remainingClusters.empty())
	{
		vector<Entry<size_t>> candidates; // all remaining clusters, sorted by key
		
		for (size_t i : remainingClusters)
		{
			// count variables that have not been conjoined yet
			size_t unprecedent = support[i].Restrict(precedent).SupportSize();
		
			size_t reoccurring = 0;
			
			for (Var v : Var::support(support[i])) // count variables that appear in other clusters
				if (occurrences[v].size() > 1)
					reoccurring++;
			
			// sort down by number of unprecendented variables, then up by number of shared variables
			vector<size_t> key = { occurrences.size() - unprecedent, reoccurring };
			candidates.emplace_back(move(key), i);
		}
	
		sort(candidates.begin(), candidates.end());
		
		size_t index = candidates.rbegin()->value;
		remainingClusters.erase(index);
		
		BDD quantifiable = m.bddOne();
		
		for (Var v : Var::support(support[index]))
		{
			vector<size_t>& occur = occurrences[v];
			occur.erase(remove(occur.begin(), occur.end(), index), occur.end());
			
			if (occur.size() > 0)
			{
				precedent &= v.bddVar();
			}
			else if (outputCube.Restrict(v.bddVar()) != outputCube)
			{
				quantifiable &= v.bddVar();
			}
		}
		
		clusters.emplace_back(specification[index], quantifiable);
	}
	
	return newObj<LinearScheduler>(move(clusters), vector<BDD>());
}

obj<ClusterScheduler> LazyClustering::run(const vector<BDD>& specification, const BDD& outputCube) const
{
	map<int, BDD, less<int>> byRank;
	BDD inputCube = m.bddOne();
	vector<BDD> assumptions;

	for (const BDD& b : specification)
	{
		BDD support = b.Support();
		BDD inputSupport = support.Restrict(outputCube);
		inputCube &= inputSupport;
		
		vector<unsigned int> outputIndices = support.Restrict(inputSupport).SupportIndices();
		
		if (outputIndices.size() == 0)
		{
			assumptions.push_back(b);
		}
		else
		{
			int rank = *max_element(outputIndices.begin(), outputIndices.end());
	
			if (byRank.find(rank) == byRank.end())
				byRank[rank] = b;
			else
				byRank[rank] &= b;
		}
	}
	
	vector<Cluster> clusters;
	BDD remainingVariables = inputCube;
	
	for (auto it = byRank.rbegin(); it != byRank.rend(); it++)
	{
		BDD cluster = it->second;
		
		BDD quantifiableSupport = cluster.Support().Restrict(remainingVariables);
		
		remainingVariables &= quantifiableSupport;
		
		clusters.push_back(Cluster(move(cluster), move(quantifiableSupport)));
	}
	
	reverse(clusters.begin(), clusters.end());
	
	return newObj<LazyScheduler>(move(clusters), move(assumptions));
}
