#include "Synthesizer.h"
#include "Mgr.h"
#include <cassert>

using std::vector;
using std::unordered_map;

BDD BoolesMethod::run(
		const BDD& specification,
		const BDD& outputCube,
		unordered_map<Var, BDD, VarHash>& witnesses)
{
	vector<BDD> pWitnesses;
	int* yIndex;
	int outputSize = outputCube.SupportSize();

	BDD pre = (!specification).SolveEqn(outputCube, pWitnesses, &yIndex, outputSize);
	
	BDD verified = (!specification).VerifySol(pWitnesses, yIndex);
	
	assert(pre == verified);
	
	VarList outputVars = Var::support(outputCube);
	vector<BDD> params(outputSize);
	
	for (int i = outputSize - 1; i >= 0; i--)
	{
		Var v_i = outputVars[i];
	
		params[i] = v_i.selectParam(selector, pWitnesses[i]);
		witnesses[v_i] = pWitnesses[i];
		
		for (int j = outputSize - 1; j >= i; j--)
		{
			Var v_j = outputVars[j];
			
			witnesses[v_i] = v_j.compose(params[j], witnesses[v_i]);
		}
	}
	
	return !pre;
}
	
obj<Synthesizer> Synthesizer::boolesMethod(obj<ParamSelector> ps)
{
	return newObj<BoolesMethod>(ps);
}
