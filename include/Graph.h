#include <unordered_set>
#include <unordered_map>

class Graph
{
	std::unordered_set<int> vertices;
	std::unordered_map<int, std::unordered_set<int>> adjacencyMap;
	
	public:
	
	Graph(std::unordered_set<int> vars);
	void addEdge(int var1, int var2);
	std::unordered_set<int>::const_iterator beginVertices() const;
	std::unordered_set<int>::const_iterator endVertices() const;
	std::unordered_set<int>::const_iterator beginNeighbors(int var);
	std::unordered_set<int>::const_iterator endNeighbors(int var);
};
