#pragma once

#include "cuddObj.hh"
#include "Mgr.h"

#include <vector>

class Instance
{
	public:
	
	Instance(const BDD& oc, std::vector<BDD> f);
	
	const BDD outputCube;
	const std::vector<BDD> factors;
	
	Instance abstract() const;
	
	//static Instance fromCNF(const CNF& cnf);
};
