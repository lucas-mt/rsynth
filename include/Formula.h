#pragma once

#include "Instance.h"
#include "Graph.h"
#include "Obj.h"

#include <vector>

class Formula
{
	protected:

	std::unordered_set<int> existentialVars;
	std::unordered_set<int> universalVars;
	
	public:
	
	Formula(std::unordered_set<int> uVars, std::unordered_set<int> eVars)
		: universalVars(std::move(uVars)), existentialVars(std::move(eVars)) {}
	
	Instance buildInstance() const;
	
	virtual Instance buildInstance(const std::unordered_map<int, BDD>& varBDD) const = 0;
	
	virtual obj<Formula> abstract() = 0; 
	
	virtual const Graph gaifmanGraph() const = 0;
};

class CNF : public Formula
{
	std::vector<std::vector<int>> clauses;
	
	public:
	
	CNF(std::unordered_set<int> uVars, std::unordered_set<int> eVars)
		: Formula(std::move(uVars), std::move(eVars)) {}
	
	void addClause(const std::vector<int>& clause);
	
	Instance buildInstance(const std::unordered_map<int, BDD>& varBDD) const;
	
	obj<Formula> abstract();
	
	const Graph gaifmanGraph() const;
};

class SupportFormula : public Formula
{
	std::vector<std::vector<int>> cubes;
	
	public:
	
	SupportFormula(std::unordered_set<int> uVars, std::unordered_set<int> eVars)
		: Formula(std::move(uVars), std::move(eVars)) {}
	
	void addCube(std::vector<int> cube);
	
	Instance buildInstance(const std::unordered_map<int, BDD>& varBDD) const;
	
	obj<Formula> abstract();
	
	const Graph gaifmanGraph() const;
};
