#pragma once

#include <memory>

template<class T>
using obj = std::shared_ptr<T>;

template<class T, class... Args>
obj<T> newObj(Args&&... args)
{
	return std::make_shared<T>(args...);
}
