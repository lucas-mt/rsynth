#pragma once

#include "cuddObj.hh"
#include "Obj.h"

class ParamSelector
{
	public:

	virtual BDD selectParam(int id, const BDD& pWitness) const = 0;
	
	static obj<ParamSelector> defaultOne();
	static obj<ParamSelector> defaultZero();
	static obj<ParamSelector> greedy();
};

class Default1Selector : public ParamSelector
{
	public:
	
	BDD selectParam(int id, const BDD& pWitness) const;
};

class Default0Selector : public ParamSelector
{
	public:
	
	BDD selectParam(int id, const BDD& pWitness) const;
};

class GreedySelector : public ParamSelector
{
	public:
	
	BDD selectParam(int id, const BDD& pWitness) const;
};
