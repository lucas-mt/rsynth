#include "Clustering.h"
#include "Ordering.h"
#include "Synthesizer.h"
#include "Instance.h"

class Command
{
	protected:
	
	obj<Formula> spec;
	obj<Ordering> ordering;
	obj<Clustering> clustering;
	obj<Synthesizer> synthesizer;
	obj<Logger> logger;
	
	Command(obj<Formula> f, obj<Ordering> o, obj<Clustering> c, obj<Synthesizer> s, obj<Logger> l)
		: spec(f), ordering(o), clustering(c), synthesizer(s), logger(l) {}

	public:
	
	virtual void execute() const = 0;

	static obj<Command> measure(obj<Formula> f, obj<Ordering> o, obj<Clustering> c, obj<Synthesizer> s, obj<Logger> l);
	
	static obj<Command> dumpDot(obj<Formula> f, obj<Ordering> o, obj<Clustering> c, obj<Synthesizer> s, obj<Logger> l);
	
	static obj<Command> test(obj<Formula> f, obj<Ordering> o, obj<Clustering> c, obj<Synthesizer> s, obj<Logger> l);
	
	static obj<Command> validate(obj<Formula> f, obj<Ordering> o, obj<Clustering> c, obj<Synthesizer> s, obj<Logger> l);
};

class MeasureCmd : public Command
{
	public:
	
	MeasureCmd(obj<Formula> f, obj<Ordering> o, obj<Clustering> c, obj<Synthesizer> s, obj<Logger> l)
		: Command(f, o, c, s, l) {}
	
	void execute() const;
};

class DumpDotCmd : public Command
{
	public:
	
	DumpDotCmd(obj<Formula> f, obj<Ordering> o, obj<Clustering> c, obj<Synthesizer> s, obj<Logger> l)
		: Command(f, o, c, s, l) {}
	
	void execute() const;
};

class TestCmd : public Command
{
	public:
	
	TestCmd(obj<Formula> f, obj<Ordering> o, obj<Clustering> c, obj<Synthesizer> s, obj<Logger> l)
		: Command(f, o, c, s, l) {}
	
	void execute() const;
};

class ValidateCmd : public Command
{
	public:
	
	ValidateCmd(obj<Formula> f, obj<Ordering> o, obj<Clustering> c, obj<Synthesizer> s, obj<Logger> l)
		: Command(f, o, c, s, l) {}
	
	void execute() const;
};
