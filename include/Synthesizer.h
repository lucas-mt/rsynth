#pragma once

#include "Var.h"
#include "Logger.h"

#include <unordered_map>

class Synthesizer
{
	protected:
	
	obj<ParamSelector> selector;
	
	Synthesizer(obj<ParamSelector> ps) : selector(ps) {}

	public:
	
	virtual BDD run(
		const BDD& specification,
		const BDD& outputCube,
		std::unordered_map<Var, BDD, VarHash>& witnesses) = 0;
	
	static obj<Synthesizer> boolesMethod(obj<ParamSelector> ps);
};

class BoolesMethod : public Synthesizer
{
	public:
	
	BoolesMethod(obj<ParamSelector> ps) : Synthesizer(ps) {}

	BDD run(
		const BDD& specification,
		const BDD& outputCube,
		std::unordered_map<Var, BDD, VarHash>& witnesses);
};
