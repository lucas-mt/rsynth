#pragma once

#include "cuddObj.hh"
#include "Synthesizer.h"
#include "Implementation.h"

#include <functional>

class Cluster
{
	public:
	
	BDD formula;
	BDD quantifiable;
	
	Cluster(BDD f, BDD q) : formula(std::move(f)), quantifiable(std::move(q)) {}
	
	static BDD quantifiableVars(const std::vector<Cluster>& clusters);
	static int nodeCount(const std::vector<Cluster>& clusters);
	static BDD totalSupport(const std::vector<Cluster>& clusters);
	static void dumpDot(const std::vector<Cluster>& clusters);
};

class ClusterScheduler
{
	public:
	
	virtual Implementation run(obj<Synthesizer> synthesizer, obj<Logger> logger) = 0;
	
	virtual std::vector<Cluster> allClusters() const = 0;
};

class LinearScheduler : public ClusterScheduler
{
	std::vector<Cluster> clusters;
	std::vector<BDD> assumptions;

	public:
	
	LinearScheduler(std::vector<Cluster> clusters_, std::vector<BDD> assumptions_);
	
	Implementation run(obj<Synthesizer> synthesizer, obj<Logger> logger);
	
	std::vector<Cluster> allClusters() const;
};

class LazyScheduler : public ClusterScheduler
{
	std::vector<Cluster> clusters;
	std::vector<BDD> assumptions;

	public:
	
	LazyScheduler(std::vector<Cluster> clusters_, std::vector<BDD> assumptions_);
	
	Implementation run(obj<Synthesizer> __unused__, obj<Logger> __also_unused__);
	
	std::vector<Cluster> allClusters() const;
};
