#pragma once

#include "ClusterScheduler.h"
#include "Obj.h"

#include <vector>

class Clustering
{
	public:
	
	virtual obj<ClusterScheduler> run(const std::vector<BDD>& specification, const BDD& outputCube) const = 0;
	
	static obj<Clustering> none();
	
	static obj<Clustering> random();
	
	static obj<Clustering> monolithic();
	
	static obj<Clustering> bouquets();
	
	static obj<Clustering> bucket();
	
	static obj<Clustering> forward();
	
	static obj<Clustering> backward();
	
	static obj<Clustering> lazy();
};

class NoClustering : public Clustering
{
	public:

	obj<ClusterScheduler> run(const std::vector<BDD>& specification, const BDD& outputCube) const;
};

class RandomReordering : public Clustering
{
	public:

	obj<ClusterScheduler> run(const std::vector<BDD>& specification, const BDD& outputCube) const;
};

class MonolithicClustering : public Clustering
{
	public:

	obj<ClusterScheduler> run(const std::vector<BDD>& specification, const BDD& outputCube) const;
};

class BouquetsClustering : public Clustering
{
	public:

	obj<ClusterScheduler> run(const std::vector<BDD>& specification, const BDD& outputCube) const;
};

class BucketClustering : public Clustering
{
	public:

	obj<ClusterScheduler> run(const std::vector<BDD>& specification, const BDD& outputCube) const;
};

class ForwardClustering : public Clustering
{
	public:

	obj<ClusterScheduler> run(const std::vector<BDD>& specification, const BDD& outputCube) const;
};

class BackwardClustering : public Clustering
{
	public:

	obj<ClusterScheduler> run(const std::vector<BDD>& specification, const BDD& outputCube) const;
};

class LazyClustering : public Clustering
{
	public:

	obj<ClusterScheduler> run(const std::vector<BDD>& specification, const BDD& outputCube) const;
};
