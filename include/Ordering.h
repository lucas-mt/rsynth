#include "Formula.h"
#include "Obj.h"

#include <unordered_map>

class Ordering
{
	public:
	
	virtual Instance buildInstance(obj<Formula> form) = 0;
	
	static obj<Ordering> native();
	static obj<Ordering> mcs();
};

class NativeOrdering : public Ordering
{
	public:
	
	Instance buildInstance(obj<Formula> form);
};

class MCSOrdering : public Ordering
{
	public:
	
	Instance buildInstance(obj<Formula> form);
};
