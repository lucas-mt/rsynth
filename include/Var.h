#pragma once

#include "ParamSelector.h"

#include <vector>
#include <set>
#include <map>

class Var
{
	unsigned int id;

	public:
	
	Var(unsigned int id_);
	
	unsigned int getId() const;
	BDD bddVar() const;
	
	bool operator==(const Var& other) const;
	bool operator<(const Var& other) const;
	
	BDD selectParam(obj<ParamSelector> ps, const BDD& pWitness) const;
	
	BDD compose(const BDD& witness, const BDD& phi) const;
	
	static std::vector<Var> support(const BDD& bdd);
};

typedef std::vector<Var> VarList;
typedef std::set<Var> VarSet;

template<class T>
using VarMap = std::map<Var, T>;

struct VarHash
{
	size_t operator()(const Var& var) const
	{
		return std::hash<unsigned int>{}(var.getId());
	}
};
