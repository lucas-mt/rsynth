#pragma once

#include "cuddObj.hh"

#include <vector>

class Result
{
	bool realizable;
	std::vector<bool> output;
	
	public:
	
	Result();
	
	Result(std::vector<bool> output_);
	
	bool isRealizable();
	
	size_t size();
	
	bool operator[](size_t i);
};
