#include "Obj.h"
#include "cuddObj.hh"

#include <vector>
#include <fstream>

class Logger
{
	protected:

	std::ostream& out;
	
	public:
	
	explicit Logger(std::ostream& os);
	
	virtual void log(const std::vector<BDD>& data) = 0;
	void log(const BDD& pre, const BDD& cluster, const BDD& conj, const BDD& quant);
	
	static obj<Logger> voidLogger(std::ostream& os);
	static obj<Logger> bddLogger(std::ostream& os);
	static obj<Logger> sizeLogger(std::ostream& os);
};

class VoidLogger : public Logger
{
	public:
	
	void log(const std::vector<BDD>& data);
	
	explicit VoidLogger(std::ostream& os);
};

class BDDLogger : public Logger
{
	public:
	
	void log(const std::vector<BDD>& data);
	
	
	
	explicit BDDLogger(std::ostream& os);
};

class SizeLogger : public Logger
{
	public:
	
	void log(const std::vector<BDD>& data);
	
	explicit SizeLogger(std::ostream& os);
};
