#pragma once

#include "cuddObj.hh"
#include "Result.h"
#include "Var.h"

#include <unordered_map>

class Implementation
{
	std::unordered_map<Var, BDD, VarHash> witnesses;
	
	public:
	
	Implementation(std::vector<BDD> pre, std::unordered_map<Var, BDD, VarHash> witn);
	
	const std::vector<BDD> precondition;
	
	const BDD& operator[](const Var& i);
	
	void dumpDot() const;
	
	int nodeCount() const;
	
	Result eval(const std::unordered_map<Var, bool, VarHash>& input) const;
};
