#pragma once

#include "Synthesizer.h"
#include "Command.h"
#include "Implementation.h"
#include "Clustering.h"
#include "DimacsReader.h"

#include <vector>
#include <unordered_map>
#include <string>

class Args
{
	std::unordered_map<std::string, std::string> args;
	
	std::string retrieve(std::string option) const;
	
	obj<Formula> getSpecification() const;
	
	obj<Ordering> getOrdering() const;
	
	obj<Clustering> getClustering() const;

	obj<ParamSelector> getParamSelector() const;
	
	obj<Logger> getLogger() const;
	
	obj<Synthesizer> getSynthesizer() const;
	
	public:
	
	obj<Command> buildCommand() const;
	
	Args(int argc, char* argv[]);
};
