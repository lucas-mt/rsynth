#pragma once

#include "Formula.h"

#include <string>

namespace DimacsReader
{
	const obj<CNF> readAEDimacs(const std::string& path);
}
