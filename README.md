# README #

### What is it? ###

**RSynth** is a tool for synthesis of Boolean functions using Binary Decision Diagrams (BDDs). Given an input Boolean formula between input and output variables, RSynth synthesizes a Boolean function from inputs to outputs that satisfies the formula for every valid input.

### Requirements ###

RSynth requires a version of g++ with support for C++11.

RSynth uses the CUDD package for BDD manipulation. CUDD version 3.0.0 or greater is required. The latest version of the package can be downloaded from [Fabio Somenzi's website](http://vlsi.colorado.edu/~fabio/).

### Installation ###

1. Build CUDD's static libraries (refer to CUDD's README for details). After this step, the file `libcudd.a` should be located in the `cudd/.libs` directory of the CUDD installation.
2. Edit RSynth's makefile (located in the `src` directory) so that the `CUDD` variable points to your installation of CUDD.
3. Run `make` from the `src` directory.

### Input format ###

Currently RSynth only supports input files in the QDIMACS format. The file must have one set of universal variables followed by one set of existential variables. Together both sets must include all variables in the formula. Universal variables are interpreted as the input and existential variables as the output.

Example:

    p cnf 20 18
    a 1 3 5 7 9 11 13 15 17 19 0
    e 2 4 6 8 10 12 14 16 18 20 0
    1 3 4 0
    -1 -3 -4 0
    3 5 6 0
    -3 -5 -6 0
    5 7 8 0
    -5 -7 -8 0
    7 9 10 0
    -7 -9 -10 0
    9 11 12 0
    -9 -11 -12 0
    11 13 14 0
    -11 -13 -14 0
    13 15 16 0
    -13 -15 -16 0
    15 17 18 0
    -15 -17 -18 0
    17 19 20 0
    -17 -19 -20 0

### Running RSynth ###

The following command-line options are required by RSynth:

* `-r` Input format. Currently only `qdimacs` is supported.
* `-s` Path to the input file. Must be in the format specified above.
* `-o` Variable ordering. Can be one of:
    - `native` Variables are ordered by their id in the input file.
    - `mcs` Variables are ordered by the Maximum Cardinality Search method on the Gaifman Graph of the formula.
* `-c` Clustering method. Determines how the clauses of the input formula are grouped and ordered. Can be one of:
    - `none` Do not group clauses.
    - `monolithic` Group all clauses into a single BDD.
    - `bouquets` Group clauses by output variable of greatest index. Process clusters in increasing order of index.
    - `bucket` Group clauses by output variable of greatest index. Process clusters in decreasing order of index.
    - `forward` Process clauses in an order that greedily maximizes the number of variables quantified in each step.
    - `backward` Process clauses in an order that greedily minimizes the size of the BDD produced in each step.
* `-m` Synthesis method. Currently only `booles` is supported.
* `-w` Function selection criterion, applied when multiple functions that satisfy the input formula exist. Can be one of:
    - `def0` Assign an output to 0 whenever possible.
    - `def1` Assign an output to 1 whenever possible.
    - `greedy` Choose locally for each output the smallest function between `def0` and `def1`.
* `-x` Execution mode. Can be one of:
    - `measure` Prints five measurements representing: time taken to construct the representation of the input formula (in milliseconds), number of factors, size of the initial formula (in BDD nodes), time taken to synthesize function (in milliseconds), size of the function (in BDD nodes).
    - `test` Randomly generates 10 input vectors and runs them through the synthesized function, printing the input and output for each test case.
    - `dump` Prints a dump of the input formula and synthesized function in the DOT format, to be read by a graph drawing program.
    - `validate` Verifies that the synthesized function correctly satisfies the input formula.

Example:

    ./rsynth -r qdimacs -s example.qdimacs -o mcs -c bouquets -m booles -w def1 -x measure

### Contact ###

Send comments or questions to [lucasmt@rice.edu](mailto:lucasmt@rice.edu).